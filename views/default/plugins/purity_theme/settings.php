<?php
	$frontleft = $vars['entity']->frontleft;
		if (!$frontleft) $frontleft='<h2>You can edit this information in the admin section of this theme.</h2>';
	$frontright = $vars['entity']->frontright;
		if (!$frontright) $frontright='<h2>You can edit this information in the admin section of this theme.</h2>';
	$showpeople = $vars['entity']->showpeople;
		if (!$showpeople) $showpeople = 'Right';
?>
<div id="habitorio_theme_admin">

		<label>Show the default "people" image?</label><br/>
		<?php echo elgg_view('input/dropdown',array('name'=>'params[showpeople]','value'=>$showpeople,'options'=>array('no','Left','Right'))); ?>
		<p>&nbsp;</p>
		<label>Front left text area:</label><br/>
		<?php
			echo elgg_view('input/longtext', array('name'=>'params[frontleft]','value'=>$frontleft));
		?>
		<p>&nbsp;</p>
		<label>Front right text area:</label><br/>
		<?php
			echo elgg_view('input/longtext', array('name'=>'params[frontright]','value'=>$frontright));
		?>

</div>
