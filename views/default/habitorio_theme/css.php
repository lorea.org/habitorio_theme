<?php
?>

#social_menu {
background:url(<?php echo $vars['url']; ?>mod/habitorio_theme/graphics/menu.png) top center no-repeat;
width:980px;
height:54px;
margin:20px auto 0px auto;
position:relative;
}
#social_topbar {
background:url(<?php echo $vars['url']; ?>mod/habitorio_theme/graphics/menu.png) bottom center no-repeat;
width:980px;
height:41px;
margin:0px auto;
padding-top:3px;
}
#social_topbar a {
color:#333333;
}
.elgg-menu-site > li > a {
padding: 6px 6px 3px 6px;
height: 20px;
font-weight:bold;
box-shadow:none;
}

.elgg-menu-site-default {
position: absolute;
bottom: 13px;
left: 30px;
height: 23px;
}
.elgg-menu-site-default > li {
float: left;
margin-right: 1px;
}
.elgg-menu-site-default li:last-child{
background:none;
}
.elgg-menu-site-default > li > a {
color: #333333;
}
.elgg-menu-site-default > .elgg-state-selected > a,
.elgg-menu-site-default > li:hover > a {
color: #fff;
-moz-border-radius:8px;
-webkit-border-radius:8px;
border-radius:8px;
-webkit-box-shadow: none;
-moz-box-shadow: none;
box-shadow: none;
background:#A7A7A7;
text-shadow:none;
}
.elgg-menu-site > li > a:hover {
text-decoration: none;
text-shadow:none;
box-shadow:inset 0 0 1px #ffffff;
}
.elgg-menu-site-more {
display: none;
position: relative;
left: -1px;
width: 100%;
z-index: 1;
min-width: 150px;
padding:0px;
background:#ffffff;
-webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;
}

li:hover > .elgg-menu-site-more {
display: block;
-webkit-box-shadow: none;
-moz-box-shadow: none;
box-shadow: none;
border:none;
background:#ffffff;
box-shadow:0px 0px 4px #333333;
text-shadow:none;
}

.elgg-menu-site-more > li > a {
color: #333;
background:#ffffff;
padding:5px;
}
.elgg-menu-site-more > li > a:hover {
background:#A7A7A7;
color:#ffffff;
text-shadow:none;
}
.elgg-menu-site-more >li:first-child> a,
.elgg-menu-site-more > li:first-child >a:hover {
-webkit-border-top-left-radius: 10px;
-webkit-border-top-right-radius: 10px;
-moz-border-radius-topleft: 10px;
-moz-border-radius-topright: 10px;
border-top-left-radius: 10px;
border-top-right-radius: 10px;
text-shadow:none;
}
.elgg-menu-site-more > li:last-child > a,
.elgg-menu-site-more > li:last-child > a:hover {
-webkit-border-bottom-right-radius: 10px;
-webkit-border-bottom-left-radius: 10px;
-moz-border-radius-bottomright: 10px;
-moz-border-radius-bottomleft: 10px;
border-bottom-right-radius: 10px;
border-bottom-left-radius: 10px;
text-shadow:none;
}
.elgg-more > a:before {
content: "\25BC";
font-size: smaller;
margin-right: 4px;
}
.elgg-page-header .elgg-search {
bottom: 49px;
height: 31px;
position: absolute;
right: 10px;
background:url(<?php echo $vars['url']; ?>mod/habitorio_theme/graphics/search.png) no-repeat;
padding-right:60px;
width:216px;
height:34px;
}
.front_left {
width:420px;
margin-left:50px;
}
.front_right {
width:420px;
margin-right:50px;
}
.search-input {
border:none;
}
.elgg-page-header .elgg-search input[type=text] {
width: 166px;
}
.elgg-page-header .elgg-search input[type=submit] {
display: none;
}
.elgg-search input[type=text] {
color: #333;
font-size: 12px;
font-weight: bold;
padding: 2px 4px 2px 26px;
margin-top:10px;
margin-left:6px;
border:none;
}
.elgg-search input[type=text]:focus, .elgg-search input[type=text]:active {

color: #0054A7;
border:none;
}
.search-list li {
padding: 5px 0 0;
}
.search-heading-category {
margin-top: 20px;
color: #666666;
}

*:hover, .elgg-state-selected {
text-shadow:none;
}
.elgg-menu-page a:hover, #profile-owner-block a:hover{
background:#333333;
}
.elgg-menu-page li.elgg-state-selected > a {
background:#000000;
}
