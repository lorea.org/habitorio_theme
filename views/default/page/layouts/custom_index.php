<?php
if (elgg_get_config('allow_registration') != false) {
	$friend_guid = (int) get_input('friend_guid', 0);
	$invitecode = get_input('invitecode');
	$register_url = elgg_get_site_url() . 'action/register';
	if (elgg_get_config('https_login')) {
		$register_url = str_replace("http:", "https:", $register_url);
	}
	$form_params = array(
		'action' => $register_url,
		'class' => 'elgg-form-account float',
	);
	$body_params = array(
		'friend_guid' => $friend_guid,
		'invitecode' => $invitecode
	);
	$content = elgg_view_form('register', $form_params, $body_params);
	$content .= elgg_view('help/register');
} else {
	$content = '';
}
$showpeople = elgg_get_plugin_setting('showpeople','habitorio_theme');
if (!$showpeople) $showpeople = 'right';
?>

<div class="custom-index elgg-main elgg-grid clearfix">
	<div class="elgg-col elgg-col-1of2">
		<div class="elgg-inner pvm phm prl">
			<div class="front_left">
				<?php if ($showpeople == 'Left') { ?>
				<img src="<?php echo $vars['url']; ?>mod/habitorio_theme/graphics/people.png">
				<?php } ?>
				<?php echo elgg_view('habitorio_theme/leftside'); ?>
				<?php if (!elgg_is_logged_in()) echo $content;?>
			</div>
		</div>
	</div>
	<div class="elgg-col elgg-col-1of2">
		<div class="elgg-inner pvm">
			<div class="front_right">
				<?php if ($showpeople == 'Right') { ?>			
				<img src="<?php echo $vars['url']; ?>mod/habitorio_theme/graphics/people.png">
				<?php } ?>
				<?php echo elgg_view('habitorio_theme/rightside'); ?>
			</div>
		</div>
	</div>
</div>
