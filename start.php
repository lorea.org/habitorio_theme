<?php

function habitorio_theme_init() {
    elgg_extend_view('page/elements/head', 'habitorio_theme/meta');
    elgg_extend_view('css/elgg', 'habitorio_theme/css');
    elgg_unregister_menu_item('topbar', 'elgg_logo');
    if (elgg_is_active_plugin(CHAMELEONTHEMEENGINE)) {
        if (!function_exists('chameleonPresets')) {

            function chameleonPresets() {
                $presets = array(
                    'preset1' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset1.png',
                        'body_background_color' => 'ECECEC',
                        'body_background_pattern' => 'eleven',
                        'accent_color' => '1d1d1d',
                        'logo_font' => 'poiret',
                        'body_font' => 'nunito',
                        'logo_color' => 'f34557'
                    ),
                    'preset2' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset2.png',
                        'body_background_color' => 'a52a2a',
                        'body_background_pattern' => 'twelve',
                        'accent_color' => '216587',
                        'logo_font' => 'oxygen',
                        'body_font' => 'ubuntu',
                        'logo_color' => '000000'
                    ),
                    'preset3' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset3.png',
                        'body_background_color' => 'e8e8e8',
                        'body_background_pattern' => 'twentyone',
                        'accent_color' => '61871f',
                        'logo_font' => 'oxygen',
                        'body_font' => 'comfortaa',
                        'logo_color' => '61871f'
                    ),
                    'preset4' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset4.png',
                        'body_background_color' => '1e1436',
                        'body_background_pattern' => 'four',
                        'accent_color' => '000000',
                        'logo_font' => 'lobster',
                        'body_font' => 'oxygen',
                        'logo_color' => '1e1436'
                    ),
                    'preset5' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset5.png',
                        'body_background_color' => '163853',
                        'body_background_pattern' => 'twentyfive',
                        'accent_color' => '0F7516',
                        'logo_font' => 'poiret',
                        'body_font' => 'nunito',
                        'logo_color' => 'ff0000'
                    ),
                    'preset6' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset6.png',
                        'body_background_color' => 'e6e3de',
                        'body_background_pattern' => 'fourteen',
                        'accent_color' => 'ea7a3e',
                        'logo_font' => 'nunito',
                        'body_font' => 'poiret',
                        'logo_color' => '4c4a47'
                    ),
                    'preset7' => array(
                        'thumbnail' => 'habitorio_theme/graphics/preset7.png',
                        'body_background_color' => '000000',
                        'body_background_pattern' => 'fifteen',
                        'accent_color' => '1d1d1d',
                        'logo_font' => 'comfortaa',
                        'body_font' => 'nunito',
                        'logo_color' => '72a32b'
                    )
                );
                return $presets;
            }

        }
    }
}

elgg_register_event_handler('init', 'system', 'habitorio_theme_init');
?>
